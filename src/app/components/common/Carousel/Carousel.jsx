// @floW
import React from 'react';
import cx from 'classnames';
import {CAROUSEL_TIMEOUT} from '../../../../constants';
import bemHelper from '../../../utils/bem';
import './carousel.scss';

const cn = bemHelper({block: 'carousel'});

export default class Carousel extends React.Component {
  state = {
    activeChild: 1,
    sliding: false
  }

  componentDidMount() {
    const numItems = React.Children.count(this.props.children);
    this.interval = setInterval(() => {
      this.setState(state => {
        return {
          activeChild: state.activeChild >= numItems - 1 ? 0 : state.activeChild + 1,
          sliding: true
        };
      }, () => {
        setTimeout(() => {
          this.setState({
            sliding: false
          });
        }, 100);
      });
    }, CAROUSEL_TIMEOUT);
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  getOrder = (itemIndex, activeIndex) => {
    const numItems = React.Children.count(this.props.children);
    if (itemIndex < activeIndex) {
      return numItems - Math.abs(itemIndex - activeIndex);
    }
    return itemIndex - activeIndex;
  }

  getChildren = () => {
    return React.Children.map(this.props.children, (child, i) => {
      return <div className={cx(
        cn('slide')
      )} style={ {order: `${this.getOrder(i, this.state.activeChild)}`} }>
        {child}
      </div>;
    });
  }

  render() {
    return <div className={cn('wrapper')}>
      <div className={cx(
        cn('container'),
        {[cn('sliding')]: this.state.sliding}
      )}>
        {this.getChildren()}
      </div>
    </div>;
  }
}
