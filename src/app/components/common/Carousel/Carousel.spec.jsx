
import React from 'react';
import {shallow} from 'enzyme';
import {CAROUSEL_TIMEOUT} from '../../../../constants';

import Carousel from './Carousel';

jest.useFakeTimers();

const render = () => {
  return shallow(<Carousel>
    <div>Hello</div>
    <div>World</div>
    <div>!!!</div>
  </Carousel>);
};

describe('Carousel', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = render();
  });
  it('should set a setInterval', () => {
    expect(wrapper.instance().interval).toBeDefined();
  });
  it('should increment active slide by 1 after timeout', () => {
    expect(wrapper.state('activeChild')).toEqual(1);
    jest.advanceTimersByTime(CAROUSEL_TIMEOUT);
    expect(wrapper.state('activeChild')).toEqual(2);
  });
  it('should set active slide by 0 after the slides have finished', () => {
    expect(wrapper.state('activeChild')).toEqual(1);
    jest.advanceTimersByTime(CAROUSEL_TIMEOUT);
    expect(wrapper.state('activeChild')).toEqual(2);
    jest.advanceTimersByTime(CAROUSEL_TIMEOUT);
    expect(wrapper.state('activeChild')).toEqual(0);
  });
  it('should clear interval on unmount', () => {
    spyOn(window, 'clearInterval');
    wrapper.unmount();
    expect(window.clearInterval).toHaveBeenCalledTimes(1);
    window.clearInterval.mockRestore();
  });
});
