// @floW
import React from 'react';
import cx from 'classnames';

import bemHelper from '../../../utils/bem';
import './ratingArc.scss';
const cn = bemHelper({ block: 'rating-arc' });

const outerRadius = 150;
const innerRadius = 135;

const svgDimension = 2 * outerRadius + 10;
const origin = outerRadius + 5;

const outerCircumference = Math.PI * 2 * innerRadius;

export const RATING_TYPE = {
  score: 'score',
  debt: 'debt'
};

export const RatingArc = ({score, maxScore, className, ratingType = RATING_TYPE.score}) => {
  const offset = outerCircumference - ((score / maxScore) * outerCircumference);
  return <svg height={svgDimension} width={svgDimension} className={className}>
    <circle
      cx={origin}
      cy={origin}
      r={outerRadius} strokeWidth={1}
      stroke="#fff"
      fill="transparent"/>
    <circle
      cx={origin}
      cy={origin}
      r={innerRadius}
      strokeWidth={4}
      strokeDasharray={outerCircumference}
      strokeDashoffset={offset}
      className={cx(cn('circle'), cn(ratingType))}
      transform={`rotate(-90 0 0)`}
      fill="transparent"/>
  </svg>;
};
