// @floW
import React from 'react';
import {MAX_CREDIT_SCORE} from '../../../../constants';
import bemHelper from '../../../utils/bem';
import './creditScore.scss';
import {RatingArc} from '../../common/RatingArc/RatingArc';

const cn = bemHelper({ block: 'credit-score' });

export const CreditScore = ({score, maxScore}) => {
  return <div className={cn('wrapper')}>
    <RatingArc score={score} maxScore={maxScore} className={ cn('arc') }/>
    <div className={cn('text')}>
      <div>Your credit score is</div>
      <span className={cn('score')}>{score}</span>
      <div>out of {MAX_CREDIT_SCORE}</div>
    </div>
  </div>;
};
