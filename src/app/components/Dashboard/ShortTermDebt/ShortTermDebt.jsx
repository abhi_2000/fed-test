// @floW
import React from 'react';
import bemHelper from '../../../utils/bem';
import './shortTermDebt.scss';
import {RATING_TYPE, RatingArc} from '../../common/RatingArc/RatingArc';

const cn = bemHelper({ block: 'debt' });

export const ShortTermDebt = ({score, maxScore}) => {
  return <div className={cn('wrapper')}>
    <RatingArc score={score} maxScore={maxScore} className={ cn('arc') } ratingType={RATING_TYPE.debt}/>
    <div className={cn('text')}>
      <div>Your short term total debt</div>
      <span className={cn('score')}>{`£${score}`}</span>
      <div>Total credit limit { `£${maxScore}` }</div>
    </div>
  </div>;
};
