import React from 'react';
import {getCreditReport} from '../../../api/credit-report';
import {MAX_CREDIT_SCORE} from '../../../constants';
import bemHelper from '../../utils/bem';
import './dashboard.scss';
import Carousel from '../common/Carousel/Carousel';
import {CreditScore} from './CreditScore/CreditScore';
import {ShortTermDebt} from './ShortTermDebt/ShortTermDebt';

const cn = bemHelper({block: 'dashboard'});

export default class Dashboard extends React.Component {
  state = {
    creditReport: null
  };

  componentDidMount() {
    getCreditReport().then(creditReport => {
      this.setState({
        creditReport
      });
    });
  }

  render() {
    if (!this.state.creditReport) {
      // show a loading state
      return <div>Loading...</div>;
    }
    const {creditReport: {creditReportInfo: {score, currentShortTermDebt, currentShortTermCreditLimit}}} = this.state;
    // Mock data has the value of credit limit for long term debt as null, so using data for short term debt
    return <div className={cn('wrapper')}>
      <Carousel>
        <CreditScore score={score} maxScore={MAX_CREDIT_SCORE}/>
        <ShortTermDebt score={currentShortTermDebt} maxScore={currentShortTermCreditLimit}/>
      </Carousel>
    </div>;
  }
};
