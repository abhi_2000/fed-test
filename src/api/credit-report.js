import request from 'superagent';

export const getCreditReport = () => {
  return request.get('https://s3.amazonaws.com/cdn.clearscore.com/native/interview_test/creditReportInfo.json')
    .then(response => response.body)
    .catch(err => {
      // console.log(err.response.statusCode);
      // return appropriate message based on status code
      // if backend returns friendly error message, using that is also an option.
    });
};
